package br.com.elotech.view;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.filechooser.FileSystemView;

import Services.CriaSchema;
import br.com.elotech.controller.Conexao;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.Writer;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;
import java.awt.event.ActionEvent;
import java.awt.Checkbox;
import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import javax.swing.JTabbedPane;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JRadioButton;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class ViewTelaPrincipal {

	private CriaSchema cria;

	private JFrame frmUtilitarioElotech;
	public JTextField coletaIpBanco;
	private JTextField coletaCaminhoBanco;
	private JTextField coletaUsuario;
	private JTextField coletaSenha;
	private JTextField coletaNomeDataBase;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	
	private String pastaPostgres;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ViewTelaPrincipal window = new ViewTelaPrincipal();
					window.frmUtilitarioElotech.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public ViewTelaPrincipal() throws IOException {
		cria = new CriaSchema();
		initialize();
	}

	private void initialize() throws IOException {
		frmUtilitarioElotech = new JFrame();
		frmUtilitarioElotech.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				saveTextValue();
			}
			@Override
			public void windowOpened(WindowEvent e) {
				loadTextValue();
			}
		});
		frmUtilitarioElotech
				.setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\eclipse\\RestauradorDeBase\\Logo.jpg"));
		frmUtilitarioElotech.setForeground(Color.GRAY);
		frmUtilitarioElotech.setTitle("Utilitarios Elotech");
		frmUtilitarioElotech.setBounds(100, 100, 613, 416);
		frmUtilitarioElotech.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmUtilitarioElotech.getContentPane().setLayout(null);
		frmUtilitarioElotech.setLocationRelativeTo(null);
		
		
	      JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
	       tabbedPane.setBounds(0, 0, 600, 375);	
	              frmUtilitarioElotech.getContentPane().add(tabbedPane, BorderLayout.NORTH);	
	           
	             JPanel frmUtilitarioElotech = new JPanel();	
	      	        tabbedPane.addTab("Restaura��o", null, frmUtilitarioElotech, null);		
	      	      frmUtilitarioElotech.setLayout(null);		
	   
		

		JLabel lblNewLabel = new JLabel("IP do Banco:");
		lblNewLabel.setBounds(18, 14, 139, 14);
		frmUtilitarioElotech.add(lblNewLabel);//.getContentPane().add(lblNewLabel);

		coletaIpBanco = new JTextField();
		coletaIpBanco.setBounds(158, 11, 228, 20);
		frmUtilitarioElotech.add(coletaIpBanco);
		//frmUtilitarioElotech.getContentPane().add(coletaIpBanco);
		coletaIpBanco.setColumns(10);

		JLabel lblDescricaoLabel = new JLabel("Caminho do Backup:");
		lblDescricaoLabel.setBounds(32, 253, 353, 14);
		frmUtilitarioElotech.add(lblDescricaoLabel);
		//frmUtilitarioElotech.getContentPane().add(lblNewLabel_1);

		coletaCaminhoBanco = new JTextField();
		coletaCaminhoBanco.setColumns(10);
		coletaCaminhoBanco.setBounds(32, 278, 422, 20);
		frmUtilitarioElotech.add(coletaCaminhoBanco);
		//frmUtilitarioElotech.getContentPane().add(coletaCaminhoBanco);

		JLabel lblUsurio = new JLabel("Usu\u00E1rio:");
		lblUsurio.setBounds(20, 72, 133, 14);
		frmUtilitarioElotech.add(lblUsurio);
		//frmUtilitarioElotech.getContentPane().add(lblUsurio);

		coletaUsuario = new JTextField();
		coletaUsuario.setBounds(158, 72, 122, 20);
		frmUtilitarioElotech.add(coletaUsuario);
		//frmUtilitarioElotech.getContentPane().add(coletaUsuario);
		coletaUsuario.setColumns(10);

		JLabel lblSenha = new JLabel("Senha:");
		lblSenha.setBounds(290, 72, 105, 14);
		frmUtilitarioElotech.add(lblSenha);
		//frmUtilitarioElotech.getContentPane().add(lblSenha);

		coletaSenha = new JTextField();
		coletaSenha.setBounds(332, 72, 122, 20);
		frmUtilitarioElotech.add(coletaSenha);
		//frmUtilitarioElotech.getContentPane().add(coletaSenha);
		coletaSenha.setColumns(10);

		JLabel lblNewLabel_2 = new JLabel("Nome Data Base:");
		lblNewLabel_2.setBounds(20, 45, 129, 14);
		frmUtilitarioElotech.add(lblNewLabel_2);
		//frmUtilitarioElotech.getContentPane().add(lblNewLabel_2);

		coletaNomeDataBase = new JTextField();
		coletaNomeDataBase.setBounds(158, 42, 228, 20);
		frmUtilitarioElotech.add(coletaNomeDataBase);
		//frmUtilitarioElotech.getContentPane().add(coletaNomeDataBase);
		coletaNomeDataBase.setColumns(10);

		JLabel lblSchemas = new JLabel("                                                                     SCHEMAS:");
		lblSchemas.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblSchemas.setBackground(new Color(0, 0, 0));
		lblSchemas.setForeground(Color.BLACK);
		lblSchemas.setBounds(-176, 149, 469, 14);
		frmUtilitarioElotech.add(lblSchemas);
		//frmUtilitarioElotech.getContentPane().add(lblSchemas);
		
		Checkbox schemaAise = new Checkbox("Aise");
		schemaAise.setFont(new Font("Dialog", Font.BOLD, 12));
		schemaAise.setBounds(32, 169, 53, 23);
		frmUtilitarioElotech.add(schemaAise);
		//frmUtilitarioElotech.getContentPane().add(schemaAise);
		
		ItemListener itemListenerAise = new ItemListener() {

			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == 1) {
					cria.setAdcionaSchemaAise(" -n aise ");
				} else {
					cria.setAdcionaSchemaAise("");
				}
			}
		};
		schemaAise.addItemListener(itemListenerAise);

		Checkbox schemaProtocolo = new Checkbox("Protocolo");
		schemaProtocolo.setState(false);
		schemaProtocolo.setFont(new Font("Dialog", Font.BOLD, 12));
		schemaProtocolo.setBounds(158, 169, 70, 22);

		ItemListener itemListenerProcolo = new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == 1) {
					cria.setAdcionaSchemaProtocolo(" -n protocolo ");
				} else {
					cria.setAdcionaSchemaProtocolo("");
				}
			}
		};
		schemaProtocolo.addItemListener(itemListenerProcolo);
		frmUtilitarioElotech.add(schemaProtocolo);
		//frmUtilitarioElotech.getContentPane().add(schemaProtocolo);

		Checkbox schemaApice = new Checkbox("Apice");
		schemaApice.setState(false);
		schemaApice.setFont(new Font("Dialog", Font.BOLD, 12));
		schemaApice.setBounds(32, 197, 53, 22);

		ItemListener itemListenerApice = new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == 1) {
					cria.setAdcionaSchemaApice(" -n apice ");
				} else {
					cria.setAdcionaSchemaApice("");
				}
			}
		};
		schemaApice.addItemListener(itemListenerApice);
		frmUtilitarioElotech.add(schemaApice);
		//frmUtilitarioElotech.getContentPane().add(schemaApice);

		Checkbox schemaEloarquivo = new Checkbox("EloArquivo");
		schemaEloarquivo.setState(false);
		schemaEloarquivo.setFont(new Font("Dialog", Font.BOLD, 12));
		schemaEloarquivo.setBounds(32, 225, 77, 22);

		ItemListener itemListenerEloArquivo = new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == 1) {
					cria.setAdcionaSchemaEloArquivo(" -n eloarquivo ");
				} else {
					cria.setAdcionaSchemaEloArquivo("");
				}
			}
		};
		schemaEloarquivo.addItemListener(itemListenerEloArquivo);
		frmUtilitarioElotech.add(schemaEloarquivo);
		//frmUtilitarioElotech.getContentPane().add(schemaEloarquivo);

		Checkbox schemaSigeloam = new Checkbox("Sigeloam");
		schemaSigeloam.setState(false);
		schemaSigeloam.setFont(new Font("Dialog", Font.BOLD, 12));
		schemaSigeloam.setBounds(158, 197, 70, 22);

		ItemListener itemListenerSigeloam = new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == 1) {
					cria.setAdcionaSchemaSigeloam(" -n Sigeloam ");
				} else {
					cria.setAdcionaSchemaSigeloam("");
				}
			}
		};
		schemaSigeloam.addItemListener(itemListenerSigeloam);
		frmUtilitarioElotech.add(schemaSigeloam);
		//frmUtilitarioElotech.getContentPane().add(schemaSigeloam);

		Checkbox schemaSigelorj = new Checkbox("Sigelorj");
		schemaSigelorj.setState(false);
		schemaSigelorj.setFont(new Font("Dialog", Font.BOLD, 12));
		schemaSigelorj.setBounds(158, 225, 70, 22);

		ItemListener itemListenerSigelorj = new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == 1) {
					cria.setAdcionaSchemaSigelorj(" -n Sigelorj ");
				} else {
					cria.setAdcionaSchemaSigelorj("");
				}
			}
		};
		schemaSigelorj.addItemListener(itemListenerSigelorj);
		frmUtilitarioElotech.add(schemaSigelorj);
		//frmUtilitarioElotech.getContentPane().add(schemaSigelorj);

		Checkbox schemaSiscop = new Checkbox("Siscop");
		schemaSiscop.setState(false);
		schemaSiscop.setFont(new Font("Dialog", Font.BOLD, 12));
		schemaSiscop.setBounds(259, 169, 70, 22);

		ItemListener itemListenerSiscop = new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == 1) {
					cria.setAdcionaSchemaSiscop(" -n Siscop ");
				} else {
					cria.setAdcionaSchemaSiscop("");
				}
			}
		};
		schemaSiscop.addItemListener(itemListenerSiscop);
		//frmUtilitarioElotech.getContentPane().add(schemaSiscop);
		
		Checkbox schemaUnico = new Checkbox("Unico");
		schemaUnico.setState(false);
		schemaUnico.setFont(new Font("Dialog", Font.BOLD, 12));
		schemaUnico.setBounds(259, 197, 70, 22);
		frmUtilitarioElotech.add(schemaUnico);
		//frmUtilitarioElotech.getContentPane().add(schemaUnico);

		ItemListener itemListenerUnico = new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == 1) {
					cria.setAdcionaSchemaUnico(" -n Unico ");
				} else {
					cria.setAdcionaSchemaUnico("");
				} 
			}
		};
		schemaSiscop.addItemListener(itemListenerUnico);
		frmUtilitarioElotech.add(schemaSiscop);
		//frmUtilitarioElotech.getContentPane().add(schemaUnico);
		
		//botao open
		JButton btnOpen = new JButton("Open..");
		btnOpen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
				JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());

				if  ((btnOpen.getText()=="Open")) {
					int returnValue = jfc.showOpenDialog(null);
					if (returnValue == JFileChooser.APPROVE_OPTION) {
					File selectedFile = jfc.getSelectedFile();
					//System.out.println(selectedFile.getAbsolutePath());	
					coletaCaminhoBanco.setText(selectedFile.getAbsolutePath());
					frmUtilitarioElotech.add(coletaCaminhoBanco);
					//frmUtilitarioElotech.getContentPane().add(coletaCaminhoBanco);
					}
				}else {
					jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
					int returnValue = jfc.showSaveDialog(null);
					File selectedFile = jfc.getSelectedFile();
					coletaCaminhoBanco.setText(selectedFile.getAbsolutePath());
					frmUtilitarioElotech.add(coletaCaminhoBanco);
				}
				
			}
		});
		btnOpen.setBounds(454, 276, 70, 23);
		frmUtilitarioElotech.add(btnOpen);
		//frmUtilitarioElotech.getContentPane().add(btnOpen);
		//Final Bot�o Open

		JButton btnExecutarAcao = new JButton("Iniciar Restaura\u00E7\u00E3o");
		frmUtilitarioElotech.add(btnExecutarAcao);
		//btnNewButton.setIcon(new ImageIcon(ViewTelaPrincipal.class.getResource("/com/sun/java/swing/plaf/windows/icons/HardDrive.gif")));
		btnExecutarAcao.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if ((cria.getAdcionaSchemaAise() == "") && (cria.getAdcionaSchemaApice() == "")
						&& (cria.getAdcionaSchemaEloArquivo() == "") && (cria.getAdcionaSchemaProtocolo() == "")
						&& (cria.getAdcionaSchemaSigeloam() == "") && (cria.getAdcionaSchemaSigelorj() == "")
						&& (cria.getAdcionaSchemaSiscop() == "") && (cria.getAdcionaSchemaUnico() == "")) {
					
					JOptionPane.showMessageDialog(null, "Como n�o foi Selecionado Nenhum Schema Ser� Feito a Restaura��o de acordo com seu Backup podendo Ser Full!!");
					
					schemaAise.setState(true);
					cria.setAdcionaSchemaApice(" -n aise ");
					
					schemaApice.setState(true);
					cria.setAdcionaSchemaAise(" -n apice ");
					
					schemaEloarquivo.setState(true);
					cria.setAdcionaSchemaApice(" -n eloarquivo");
					
					schemaProtocolo.setState(true);
					cria.setAdcionaSchemaProtocolo(" -n protocolo ");
					
					schemaSigeloam.setState(true);
					cria.setAdcionaSchemaSigeloam(" -n sigeloam ");
					
					schemaSigelorj.setState(true);
					cria.setAdcionaSchemaSigelorj(" -n sigelorj ");
					
					schemaSiscop.setState(true);
					cria.setAdcionaSchemaSiscop(" -n siscop ");
					
					schemaUnico.setState(true);
					cria.setAdcionaSchemaSiscop(" -n unico ");
					}
				
				try {

					if (btnOpen.isSelected()) { 
					Conexao con = new Conexao(coletaIpBanco.getText(), coletaUsuario.getText().toLowerCase(), coletaSenha.getText().toLowerCase(),
							coletaCaminhoBanco.getText(), coletaNomeDataBase.getText().toUpperCase(), cria);
					System.out.println(con);
					String path = System.getProperty("user.dir");
					String localDoArquivo = path + "\\restaura.bat";
					FileWriter arq = new FileWriter(localDoArquivo);
					PrintWriter gravarArq = new PrintWriter(arq);
					
					JOptionPane.showMessageDialog(null, "Foi gerado um arquivo .bat no Seguinte Caminho "
							+ localDoArquivo + " \nA restaura��o Ir� Iniciar em Instantes");

					
					gravarArq.printf("set pguser=" + coletaUsuario.getText().toLowerCase() + " " + "\nset pgpassword="
							+ coletaSenha.getText().toLowerCase() + "\npg_restore -h " + coletaIpBanco.getText() + " -U "
							+ coletaUsuario.getText() + " -p 5432 " + cria.getAdcionaSchemaAise()
							+ cria.getAdcionaSchemaApice() + cria.getAdcionaSchemaEloArquivo()
							+ cria.getAdcionaSchemaProtocolo() + cria.getAdcionaSchemaSigeloam()
							+ cria.getAdcionaSchemaSigelorj() + cria.getAdcionaSchemaSiscop() + cria.getAdcionaSchemaUnico() + "--verbose -d "
							+ coletaNomeDataBase.getText().toUpperCase() + " " + "\"" + coletaCaminhoBanco.getText()
							+ "\"" + "\nPAUSE");
					arq.close();
					java.awt.Desktop.getDesktop().open(new File(localDoArquivo)); //chamdo o bat
					System.exit(0);
					}else {
						Conexao backup = new Conexao(pastaPostgres);
						backup.createBackupCommand(coletaUsuario.getText().toLowerCase(),
								coletaSenha.getText().toLowerCase(), coletaIpBanco.getText(),
								coletaNomeDataBase.getText().toUpperCase(),  cria.getAdcionaSchemaAise()
								+ cria.getAdcionaSchemaApice() + cria.getAdcionaSchemaEloArquivo()
								+ cria.getAdcionaSchemaProtocolo() + cria.getAdcionaSchemaSigeloam()
								+ cria.getAdcionaSchemaSigelorj() + cria.getAdcionaSchemaSiscop() + cria.getAdcionaSchemaUnico(), coletaCaminhoBanco.getText());						
						
					}
				} catch (IOException | SQLException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnExecutarAcao.setBounds(191, 310, 178, 23);
		
		frmUtilitarioElotech.add(btnOpen);
		
		JButton btnNewButton_1 = new JButton("Testar Conex\u00E3o");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Conexao c = new Conexao();
				c.testarConexao(coletaIpBanco.getText(), 
						coletaUsuario.getText(), 
						coletaSenha.getText());
			}
		});
		btnNewButton_1.setBounds(223, 103, 131, 23);
		frmUtilitarioElotech.add(btnNewButton_1);
		
		
		JRadioButton rdbtnBackup = new JRadioButton("Backup");
		rdbtnBackup.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if (rdbtnBackup.isSelected()) {
					btnOpen.setText("Salvar");
					btnExecutarAcao.setText("Fazer Backup");
					lblDescricaoLabel.setText("Local Salvar Arquivo:");
					
				} else {
					btnOpen.setText("Open");
					btnExecutarAcao.setText("Iniciar Restaura��o");
					lblDescricaoLabel.setText("Caminho do Arquivo:");
				}
			}
		});
		buttonGroup.add(rdbtnBackup);
		rdbtnBackup.setMnemonic('1');
		rdbtnBackup.setBounds(396, 169, 109, 23);
		frmUtilitarioElotech.add(rdbtnBackup);
		
		JRadioButton rdbtnRestaurar = new JRadioButton("Restaurar");
		rdbtnRestaurar.setSelected(true);
		rdbtnRestaurar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if (rdbtnRestaurar.isSelected()) {
					btnOpen.setText("Open");
					btnExecutarAcao.setText("Iniciar Restaura��o");
					lblDescricaoLabel.setText("Caminho do Arquivo:");
				} else {
					btnOpen.setText("Salvar");
					btnExecutarAcao.setText("Fazer Backup");
					lblDescricaoLabel.setText("Local Salvar Arquivo:");
				}
			}
		});
		buttonGroup.add(rdbtnRestaurar);
		rdbtnRestaurar.setMnemonic('2');
		rdbtnRestaurar.setBounds(396, 197, 109, 23);
		frmUtilitarioElotech.add(rdbtnRestaurar);
		
	}
	
	private void saveTextValue()
	{
		Properties p1 = new Properties();
        try {
        	String path = System.getProperty("user.dir");
            Writer propWriter = new BufferedWriter(new FileWriter(path + "/utilitario.app"));
            
            p1.setProperty("IpBanco", coletaIpBanco.getText());
            p1.setProperty("NomeDataBase", coletaNomeDataBase.getText());
            p1.setProperty("Usuario", coletaUsuario.getText());
            p1.setProperty("Senha", coletaSenha.getText());
          	p1.setProperty("PastaPostgres", this.pastaPostgres);
            
            p1.store(propWriter, "UtilitarioElotech");
        } catch (IOException e) {
            e.printStackTrace();
        }
	}
	
	private void loadTextValue()
	{
        Properties p1 = new Properties();
        try {
        	String path = System.getProperty("user.dir");
            InputStream is = new FileInputStream(path + "/utilitario.app");
            p1.load(is);
            
            coletaIpBanco.setText(p1.getProperty("IpBanco"));
            coletaNomeDataBase.setText(p1.getProperty("NomeDataBase"));
            coletaUsuario.setText(p1.getProperty("Usuario"));
            coletaSenha.setText(p1.getProperty("Senha"));            
            pastaPostgres = p1.getProperty("PastaPostgres");
            if (pastaPostgres == null) {
            	pastaPostgres = "";
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
	}


}
